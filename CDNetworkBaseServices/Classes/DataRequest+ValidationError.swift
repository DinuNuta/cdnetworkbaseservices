//
//  DataRequest+ValidationError.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/19/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import Alamofire

extension DataRequest {
    public func validateErrors() -> Self {
        return validate { (request, response, data) -> ValidationResult in
            if response.statusCode >= 300,
                let error = try? ErrorResponse(data: data)
                 {
                return .failure(error)
            }
            return .success({self.validate(statusCode: 200..<300)}())
        }//.validate(statusCode: 200..<300)
    }
}
