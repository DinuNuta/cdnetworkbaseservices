//
//  KeyPathSerializer.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/17/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import Alamofire


/// `AlamofireDecodableError` is the error type returned by CodableAlamofire.
///
/// - invalidKeyPath:   Returned when a nested dictionary object doesn't exist by special keyPath.
/// - emptyKeyPath:     Returned when a keyPath is empty.
/// - invalidJSON:      Returned when a nested json is invalid.
public enum AlamofireDecodableError: Error {
    case invalidKeyPath
    case emptyKeyPath
    case invalidJSON
}

extension AlamofireDecodableError: LocalizedError {
    
    public var errorDescription: String? {
        switch self {
        case .invalidKeyPath:   return "Nested object doesn't exist by this keyPath."
        case .emptyKeyPath:     return "KeyPath can not be empty."
        case .invalidJSON:      return "Invalid nested json."
        }
    }
}


final class KeyPathSerializer<SerializedObject: Decodable>: DataResponseSerializerProtocol {

    private let keyPath: String?
    private let decoder: JSONDecoder

    init(keyPath: String?, decoder: JSONDecoder = JSONDecoder()) {
        self.keyPath = keyPath
        self.decoder = decoder
    }

    func serialize(request: URLRequest?, response: HTTPURLResponse?, data: Data?, error: Error?) throws -> SerializedObject {
        if let error = error {
            throw error
        }

        if let keyPath = self.keyPath {
            if keyPath.isEmpty {
                throw AFError.responseSerializationFailed(reason: .decodingFailed(error: AlamofireDecodableError.emptyKeyPath))
            }

            let json = try JSONResponseSerializer().serialize(request: request, response: response, data: data, error: error)
            if let nestedJson = (json as AnyObject).value(forKeyPath: keyPath) {
                guard JSONSerialization.isValidJSONObject(nestedJson) else {
                    throw AFError.responseSerializationFailed(reason: .decodingFailed(error: AlamofireDecodableError.invalidJSON))
                }
                let data = try JSONSerialization.data(withJSONObject: nestedJson)
                let object = try decoder.decode(SerializedObject.self, from: data)
                return object
            }
            else {
                throw AFError.responseSerializationFailed(reason: .decodingFailed(error: AlamofireDecodableError.invalidKeyPath))
            }
        } else {
            let data = try DataResponseSerializer.init(emptyRequestMethods: [.head,.delete]) .serialize(request: request, response: response, data: data, error: error)
            let object = try self.decoder.decode(SerializedObject.self, from: data)
            return object
        }
    }
}
