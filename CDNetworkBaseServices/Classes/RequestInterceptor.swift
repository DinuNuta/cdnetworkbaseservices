//
//  RequestInterceptor.swift
//  EBSChatiOS
//
//  Created by Dinu Coscodan on 4/16/20.
//

import Foundation
import Alamofire

public protocol AccessTokenStorage {
    typealias JWT = String
    var accessToken: JWT { get set }
    var refreshToken: JWT? { get set }
    var appToken: [String:String]? { get set }
}

class KeychainStorage: AccessTokenStorage{
    var accessToken: JWT = "null"
    var appToken: [String:String]?
    var refreshToken: JWT?
}


open class RequestInterceptor: Alamofire.RequestInterceptor {

    public var storage: AccessTokenStorage

    public init(storage: AccessTokenStorage) {
        self.storage = storage
    }

    public func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
//        guard urlRequest.url?.absoluteString.hasPrefix("https://api.authenticated.com") == true else {
//            /// If the request requires authentication, we can directly return it as unmodified.
//            return completion(.success(urlRequest))
//        }
        var urlRequest = urlRequest
        /// Set the Authorization header value using the access token.
        urlRequest.setValue("Bearer " + storage.accessToken, forHTTPHeaderField: "Authorization")
        storage.appToken?.forEach({ (key,value) in
            urlRequest.setValue(value, forHTTPHeaderField: key)
        })
        completion(.success(urlRequest))
    }

    public func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 else {
            /// The request did not fail due to a 401 Unauthorized response.
            /// Return the original error and don't retry the request.
            return completion(.doNotRetry)
//            return completion(.doNotRetryWithError(error))
        }
        
        refreshToken(error: error) { [weak self] result in
            guard let self = self else { return }

            switch result {
            case .success(let token):
                self.storage.accessToken = token
                /// After updating the token we can safily retry the original request.
                completion(.retry)
            case .failure(let error):
                completion(.doNotRetryWithError(error))
            }
        }
    }
    
    open func refreshToken(error: Error, completion: @escaping (Result<String,Error>)->Void) {
        //Should override to implement the refresh token request
         completion(.failure(error))
    }
    
}
