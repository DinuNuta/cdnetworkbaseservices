//
//  CodableExtenstion.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/19/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
extension Decodable {
    init(data: Data?, keyPath: String? = nil, decoder:JSONDecoder = apiJSONDecoder) throws {
        guard let data = data else {
            let context = DecodingError.Context.init(codingPath: [], debugDescription: "Didn't found object for keypath : \(String(describing: keyPath))")
            throw DecodingError.valueNotFound(Self.self, context)
        }
        
        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else {
                    let context = DecodingError.Context.init(codingPath: [], debugDescription: "Didn't found object for keypath : \(keyPath)")
                    throw DecodingError.valueNotFound(Self.self, context)
            }
            self = try decoder.decode(Self.self, withJSONObject: nestedJson)
            return
        }
        self = try decoder.decode(Self.self, from: data)
    }
}

extension Array where Element: Codable {
    init(data: Data?, keyPath: String? = nil, decoder:JSONDecoder = apiJSONDecoder) throws {
        guard let data = data else {
            let context = DecodingError.Context.init(codingPath: [], debugDescription: "Didn't found object for keypath : \(String(describing: keyPath))")
            throw DecodingError.valueNotFound(Self.self, context)
        }
        
        if let keyPath = keyPath {
            let root = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
            guard let nestedJson = (root as AnyObject).value(forKeyPath: keyPath)
                else {
                    let context = DecodingError.Context.init(codingPath: [], debugDescription: "Didn't found object for keypath : \(keyPath)")
                    throw DecodingError.valueNotFound(Self.self, context)
            }
            self = try decoder.decode([Element].self, withJSONObject: nestedJson)
            return
        }
        self = try decoder.decode([Element].self, from: data)
    }
}

/// A decoder with:
/// dateDecodingStrategy = .iso8601
/// keyDecodingStrategy = .convertFromSnakeCase
///
/// - Returns: JSONDecoder
var apiJSONDecoder : JSONDecoder {
    let decoder = JSONDecoder()
    decoder.keyDecodingStrategy = RequestConfig.keyDecodingStrategy
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = RequestConfig.dateDecodingStrategy
    }
    return decoder
}

/// A encoder with:
/// dateEncodingStrategy = .iso8601
/// keyEncodingStrategy = .convertToSnakeCase
///
/// - Returns: JSONEncoder
var apiJSONEncoder: JSONEncoder {
    let encoder = JSONEncoder()
    encoder.keyEncodingStrategy = RequestConfig.keyEncodingStrategy
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = RequestConfig.dateEncodingStrategy
    }
    return encoder
}

public extension JSONEncoder {
    func encodeJSONObject<T: Encodable>(_ value: T, options opt: JSONSerialization.ReadingOptions = []) throws -> Any {
        let data = try encode(value)
        return try JSONSerialization.jsonObject(with: data, options: opt)
    }
}

public extension JSONDecoder {
    func decode<T: Decodable>(_ type: T.Type, withJSONObject object: Any, options opt: JSONSerialization.WritingOptions = []) throws -> T {
        let data = try JSONSerialization.data(withJSONObject: object, options: opt)
        return try decode(T.self, from: data)
    }
}

public extension Encodable {
    var dictionary: [String: Any]? {
        do {
            return try apiJSONEncoder.encodeJSONObject(self, options: .allowFragments) as? [String: Any]
        } catch {
            return nil
        }
    }
    
    var data: Data? {
        do{
            return try apiJSONEncoder.encode(self)
        }catch{
            return nil
        }
    }
}
