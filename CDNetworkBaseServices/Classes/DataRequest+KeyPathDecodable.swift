//
//  DataRequest+KeyPathDecodable.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/17/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import Alamofire

extension DataRequest {

    /// Adds a handler to be called once the request has finished.
     
    /// - parameter queue:             The queue on which the completion handler is dispatched. Default: `.main`.
    /// - parameter keyPath:           The keyPath where object decoding should be performed. Default: `nil`.
    /// - parameter decoder:           The decoder that performs the decoding of JSON into semantic `Decodable` type. Default: `JSONDecoder()`.
    /// - parameter completionHandler: The code to be executed once the request has finished and the data has been mapped by `JSONDecoder`.
     
    /// - returns: The request.
    
    @discardableResult
    public func responseDecodable<T: Decodable>(queue: DispatchQueue = .global(qos: .utility),
                                                      keyPath: String? = nil,
                                                      decoder: JSONDecoder = JSONDecoder(),
                                                      completionHandler: @escaping (AFDataResponse<T>) -> Void) -> Self {
        return response(queue: queue,
                        responseSerializer: KeyPathSerializer<T>(keyPath: keyPath, decoder: decoder),
                        completionHandler: completionHandler)
    }
}
