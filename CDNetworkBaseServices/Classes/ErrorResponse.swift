//
//  ErrorInterpreter.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/19/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
public struct ErrorResponse: Error, Codable {
    public var status: Int?
    public var message: String?
    public var error: String?
    public let title: String?
    public var errors: [ValidationErrorResponse]?
    public var stack: String?
    
    public var localizedDescripiton: String {
        if let validationError = errors?.reduce("", { (_, validationError) -> String? in
            return validationError.localizedDescripiton.flatMap({ $0 + "\n"})
        }) {
            return  (message?.localizedLowercase ?? "") + validationError
        }

        if let errorDescription = error?.localizedLowercase {
            return errorDescription
        }

        if let description = message?.localizedLowercase {
            return description
        }
        return ""
    }
    
    public var debugDescription: String? {
        return error?.debugDescription ?? errors?.compactMap({$0.debugDescription}).joined(separator: "+ /n") ?? stack
    }
}

public struct ValidationErrorResponse: Error, Codable {
    public var code: Int?
    public var message: String?
    public var type: String?
    public var field: String?
    
    public var localizedDescripiton: String? {
        return message?.localizedLowercase
    }

   public var debugDescription: String? {
        return (field.flatMap({"KeyField: \($0)"}) ?? "") + (message.flatMap({" \($0.localizedLowercase) + \n"}) ?? "")
    }
}
