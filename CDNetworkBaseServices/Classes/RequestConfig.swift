//
//  RequestConfig.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/19/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation

public struct RequestConfig {
    public static var payLoadPath: String?
    public static var keyDecodingStrategy : JSONDecoder.KeyDecodingStrategy = .convertFromSnakeCase
    public static var keyEncodingStrategy : JSONEncoder.KeyEncodingStrategy = .convertToSnakeCase
    public static var dateDecodingStrategy : JSONDecoder.DateDecodingStrategy = .iso8601
    public static var dateEncodingStrategy : JSONEncoder.DateEncodingStrategy = .iso8601
    
}
