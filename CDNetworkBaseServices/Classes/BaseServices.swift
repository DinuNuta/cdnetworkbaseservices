//
//  BaseServices.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/17/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import Alamofire
//typealias Result<T> = Swift.Result<T,Error>

public protocol BaseServices {
    var session: Alamofire.Session { get }
    var payloadPath: String? { get }
    var decoder: JSONDecoder { get }

    @discardableResult func requestTask<T: Decodable>(_ request: RequestHttp, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTask?
    @discardableResult func requestPayloadedTask<T: Decodable>(_ request: RequestHttp, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTask?
    @discardableResult func requestTask<T: Decodable>(_ request: RequestHttp, keyPath: String?, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTask?
    @discardableResult func requestJson(_ request: RequestHttp, completion: @escaping (Result<Any, Error>) -> Void) -> URLSessionTask?
    
    @discardableResult func upload<T: Decodable>(request: RequestMultipartData, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<T?,Error>) -> Void) -> URLSessionTask?
    @discardableResult func uploadPayloadedResp<T: Decodable>(request: RequestMultipartData, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<T?,Error>) -> Void) -> URLSessionTask?
    @discardableResult func upload<T: Decodable>(request: RequestMultipartData, keyPath: String?, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<T?,Error>) -> Void) -> URLSessionTask?
    @discardableResult func uploadJsonResp(request: RequestMultipartData, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<Any, Error>) -> Void) -> URLSessionTask?
}

struct Payload<T:Decodable> : Decodable {
    var status: Int?
    var data: T
}
//please use simple requestTask instead requestPayloadedTask can not be empty 
//extension Payload: EmptyResponse {
//    static func emptyValue() -> Payload<T> {
//        guard let emptyResponseType = T.self as? EmptyResponse.Type, let emptyValue = emptyResponseType.emptyValue() as? T else {
//            return Payload(status: nil, data: EmptyResp() as! T)
//        }
//        return Payload(status: nil, data: emptyValue)
//    }
//}

public extension BaseServices {
    
    var session: Alamofire.Session {
        return Authorised.session
    }
    
    var payloadPath: String? {
        return RequestConfig.payLoadPath
    }
    ///a json  decoder for decoding response
    var decoder: JSONDecoder {
        return apiJSONDecoder
    }
    
    //MARK: - Request
    /// response extract nested data
    @discardableResult
    func requestTask<T: Decodable>(_ request: RequestHttp, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTask? {
        return session.request(request)
            .validateErrors()
            .responseDecodable(of: T.self,
                               queue: DispatchQueue.global(qos: .utility),
                               decoder: decoder,
                               emptyRequestMethods: [.head,.delete],
                               completionHandler:  { (response) in
                                completion(response.tryMap({$0}).result)
            }).task
    }
    
    
    /// response extract nested data
    @discardableResult
    func requestPayloadedTask<T: Decodable>(_ request: RequestHttp, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTask? {
        return session.request(request)
            .validateErrors()
            .responseDecodable(of: Payload<T>.self,
                               queue: DispatchQueue.global(qos: .utility),
                               decoder: decoder,
                               emptyRequestMethods: [.head,.delete],
                               completionHandler:  { (response) in
                                completion(response.tryMap({$0.data}).result)
            }).task
    }
    
    /// response extract nested data by keypath
    @discardableResult
    func requestTask<T: Decodable>(_ request: RequestHttp, keyPath: String?, completion: @escaping (Result<T, Error>) -> Void) -> URLSessionTask? {
        return session.request(request)
            .validateErrors()
            .responseDecodable(keyPath: keyPath, decoder: decoder, completionHandler: { (response) in
                completion(response.tryMap({$0}).result)
            }).task
    }
    @discardableResult
    func requestJson(_ request: RequestHttp, completion: @escaping (Result<Any, Error>) -> Void) -> URLSessionTask? {
        return session.request(request)
            .validateErrors()
            .responseJSON(queue: .global(qos: .utility), completionHandler: { (response) in
                completion(response.tryMap({$0}).result)
            }).task
    }
    
    
    //MARK: - Upload
    @discardableResult
    func upload<T: Decodable>(request: RequestMultipartData, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<T,Error>) -> Void) -> URLSessionTask? {
        return prepareUpload(request: request)
            .uploadProgress { (progress) in
                progressHandler?(progress)}
            .responseDecodable(of: T.self,
                               queue: DispatchQueue.global(qos: .utility),
                               decoder: decoder,
                               emptyRequestMethods: [.head,.delete],
                               completionHandler:  { (response) in
                                completion(response.tryMap({$0}).result)})
            .task
    }
    
    @discardableResult
    func uploadPayloadedResp<T: Decodable>(request: RequestMultipartData, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<T,Error>) -> Void) -> URLSessionTask? {
        return prepareUpload(request: request)
            .uploadProgress { (progress) in
                progressHandler?(progress)}
            .responseDecodable(of: Payload<T>.self,
                               queue: DispatchQueue.global(qos: .utility),
                               decoder: decoder,
                               emptyRequestMethods: [.head,.delete],
                               completionHandler:  { (response) in
                                completion(response.tryMap({$0.data}).result)})
            .task
    }
    
    @discardableResult
    func upload<T: Decodable>(request: RequestMultipartData, keyPath: String?, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<T,Error>) -> Void) -> URLSessionTask? {
        return prepareUpload(request: request)
            .uploadProgress { (progress) in
                progressHandler?(progress)}
            .responseDecodable(keyPath: keyPath, decoder: decoder) { (response) in
                completion(response.tryMap({$0}).result)}
            .task
    }
    
    @discardableResult
    func uploadJsonResp(request: RequestMultipartData, progressHandler: ((Progress) -> Void)?, completion: @escaping (Result<Any,Error>) -> Void) -> URLSessionTask? {
        return prepareUpload(request: request)
            .uploadProgress { (progress) in
                progressHandler?(progress)}
            .responseJSON(queue: .global(qos: .utility) , completionHandler: { (response) in
                completion(response.tryMap({$0}).result)
            }).task
    }
    
    func prepareUpload(request: RequestMultipartData) -> UploadRequest {
        return session.upload(multipartFormData: { multipartFormData in
            if let files = request.files{
                files.forEach { (file) in
                    let uuid = UUID().uuidString
                    guard let _url = URL.init(string: file.filesPath) else {return}
                    multipartFormData.append(_url, withName: "files", fileName: uuid, mimeType: file.mimeType.rawValue)
                }
            }
            if let data = request.data {
                let uuid = UUID().uuidString
                multipartFormData.append(data, withName: "files", fileName: uuid, mimeType: request.mimeType?.rawValue)
            }
            request.parameters?.forEach({
                guard let valueString = $0.value as? String else { return }
                guard let valueData = valueString.data(using: .utf8) else { return }
                multipartFormData.append(valueData, withName: $0.key)
            })
        }, to: request.url)
    }
    
    //MARK: - Download
    //TODO: add download file request
}
