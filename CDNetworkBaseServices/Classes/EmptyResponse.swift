//
//  EmptyResponse.swift
//  CDNetworkBaseServices
//
//  Created by Dinu Coscodan on 6/4/20.
//

import Foundation
import Alamofire
public struct EmptyResp: Codable {
    /// Static `Empty` instance used for all `Empty` responses.
    public static let value = EmptyResp()
}

extension EmptyResp: EmptyResponse {
    public static func emptyValue() -> EmptyResp {
        value
    }
}
