//
//  Request.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/17/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import Alamofire

public struct RequestHttp: URLRequestConvertible {

    let method: Alamofire.HTTPMethod
    let url: URL
    let parameters: [String: Any]?
    let encoding: ParameterEncoding
    let headers: [String: String]?
    
    public init(url: URL,
         method: Alamofire.HTTPMethod = .get,
         parameters: [String: Any]? = nil,
         encoding: ParameterEncoding = URLEncoding.default,
         headers: [String: String]? = nil) {

        self.url = url
        self.method = method
        self.parameters = parameters
        self.encoding = encoding
        self.headers = headers
        
    }
    
    public func asURLRequest() throws -> URLRequest {
        let _headers = headers.flatMap({ HTTPHeaders($0) })
        let request = try URLRequest(url: url, method: method, headers: _headers)
        return try encoding.encode(request, with: parameters)
    }
}

public struct RequestMultipartData {
    let method: Alamofire.HTTPMethod
    let url: URL
    let parameters: [String: Any]?
    let encoding: ParameterEncoding
    let headers: [String: String]?
    let data: Data?
    let mimeType: FileMimeType?
    let files: [File]?

    public init(url: URL, method: Alamofire.HTTPMethod = .get,
         parameters: [String: Any]? = nil,
         headers: [String: String]? = nil,
         encoding: ParameterEncoding = JSONEncoding.default,
         data: Data? = nil,
         files: [File]? = nil,
         mimeType: FileMimeType? = nil) {

        self.url = url
        self.method = method
        self.parameters = parameters
        self.encoding = encoding
        self.headers = headers
        self.data = data
        self.files = files
        self.mimeType = mimeType
    }
    public func asURLRequest() throws -> URLRequest {
        let _headers = headers.flatMap({ HTTPHeaders($0) })
        let request = try URLRequest(url: url, method: method, headers: _headers)
        return try encoding.encode(request, with: parameters)
    }
}

public struct File {
    let filesPath: String
    let mimeType: FileMimeType
    
    public init(filesPath: String, mimeType: FileMimeType){
        self.filesPath = filesPath
        self.mimeType = mimeType
    }
}

public enum FileMimeType: String {
    case image = "image/jpg"
    case video = "video/mov"
    case audio = "audio/m4a"
    case pdf = "application/pdf"
}
