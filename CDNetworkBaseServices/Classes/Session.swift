//
//  Session.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/16/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import Alamofire

/// Authorized request session
public struct Authorised {
    public static var tokenStorage: AccessTokenStorage = KeychainStorage()
    public static var requestInterceptor : Alamofire.RequestInterceptor = RequestInterceptor(storage: Authorised.tokenStorage)
    public static let session: Session = {
        return Session(interceptor: Authorised.requestInterceptor)
    }()
}
