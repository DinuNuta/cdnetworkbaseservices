//
//  URLs.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/19/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
public struct URLs {
    static var base = URL(string: "https://chat.api.dev.ebs.io/")!
    static var api = base.appendingPathComponent("api/v1.0/")
    static var conversations = api.appendingPathComponent("conversations")
    static var product = api.appendingPathComponent("product")
    
    
    static func messages(by conversationId: String) -> URL {
        conversations.appendingPathComponent("\(conversationId)/messages")
    }
    static func message(id:String, conversationId: String) -> URL {
        conversations.appendingPathComponent("\(conversationId)/messages/\(id)")
    }

}

public extension URL {
    func appendingQuery(withParameters parameters: [URLQueryItem]) -> URL {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        if components.queryItems != nil {
            components.queryItems?.append(contentsOf: parameters)
        } else {
            components.queryItems = parameters }
        return components.url!
    }

    var queryItems: [URLQueryItem] {
        let components = URLComponents(url: self, resolvingAgainstBaseURL: true)!
        return components.queryItems ?? []
    }

}
