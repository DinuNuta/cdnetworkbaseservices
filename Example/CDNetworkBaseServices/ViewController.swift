//
//  ViewController.swift
//  CDNetworkBaseServices
//
//  Created by DinuNuta on 04/15/2020.
//  Copyright (c) 2020 DinuNuta. All rights reserved.
//

import UIKit
import CDNetworkBaseServices

class ViewController: UIViewController {

    let service : TestService = TestServiceImp()
    
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        testGetApi()
    }
    
    func testGetApi(){
        service.getListEntity { (result) in
            do{
                let messages = try result.get()
                print(messages)
            }catch{
                print(error.asAFError.debugDescription)
                if case .responseValidationFailed(reason: .customValidationFailed(error: let _error)) = error.asAFError {
                    if let errorResponse = _error as? ErrorResponse {
                        print(errorResponse.localizedDescripiton)
                    }
                }
            }
        }
    }
    
    func testSendFile(){
        let conversationID = "5edfaa0d4639aa0020154ed9"
        guard let urlPath = Bundle.main.url(forResource: "Dinu_SpaceXTestApp", withExtension: "mov")?.absoluteString
            else{return}
        print(urlPath)
        service.sendfile(path: urlPath, on: conversationID) { (result) in
            do{
                let message = try result.get()
                print(message)
            }catch{
                print(error)
            }
        }
    }
    @IBAction func tapSendFile(_ sender: Any) {
        testSendFile()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


