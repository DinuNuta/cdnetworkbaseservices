//
//  TestService.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/17/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import CDNetworkBaseServices

struct Payload<T:Decodable> : Decodable {
    var status: String
    var data: T
}

struct TestModel: Codable {
    var status: String
}

protocol TestService {
    func getListEntity(completion: @escaping (Result<[ChatMessage],Error>)->Void)
    func sendfile(path:String, on convId:String, completion: @escaping (Result<ChatMessage,Error>)->Void)
}

struct TestServiceImp: TestService, BaseServices {
    
    
    func getListEntity(completion: @escaping (Result<[ChatMessage],Error>) -> Void) {
        let conversationID = "5edfaa0d4639aa0020154ed9"
        let _url = URLs.messages(by: conversationID)
        let request = RequestHttp(url: _url)
        
        requestPayloadedTask(request, completion: completion)
        
//        let _url = URLs.conversations.appendingPathComponent(conversationID)
//        let request = RequestHttp(url: _url, method: .delete)
//        requestTask(request) { (result:Result<EmptyResp, Error>) in
//            print(result)
//        }
//        requestTask(request, keyPath: payloadPath, completion: completion)
    }
        
    func sendfile(path: String, on convId: String, completion: @escaping (Result<ChatMessage, Error>) -> Void) {
        var _url = URLs.messages(by: convId)
//        let urlPath = Bundle.main.url(forResource: "warning_icon_norm@2x", withExtension: "png")!.absoluteString
        let file = File(filesPath: path, mimeType: .video)
        
        let populate : [PopulateMessage] = [.attachments,.owner,.embeds]
        let _populate = populate.map({$0.rawValue}).joined(separator: ",")
        
        _url = _url.appendingQuery(withParameters: [URLQueryItem(name: "populate", value: _populate)])
        
        let request = RequestMultipartData(url: _url, method: .post, parameters: ["conntent":"", "tag":"tag" ], files: [file])
        uploadPayloadedResp(request: request, progressHandler: { (progress) in
            print(progress)
        }, completion: completion)
        
//        upload(request: request, progressHandler: { (progress) in
//            print(progress)
//        }, completion: completion)
        
//        uploadJsonResp(request: request, progressHandler: { (progress) in
//            print(progress)
//        }) { (result) in
//            do{
//                let resp = try result.get()
//                print(resp)
//            }catch{
//                print(error)
//            }
//        }
    }
    
}
