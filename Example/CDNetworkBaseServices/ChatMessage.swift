
//
//  ChatMessage.swift
//  ChatModule
//
//  Created by Dinu Coscodan on 3/31/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation

struct ChatMessage: Codable {
    let id: String
    let conversation: String?
    let tag: String
    let content: String
    let timestamp: Date
    let type: Int
    let owner: String?
    let pool: Pool?
    let embeds: [String]
    //let embeds: [Embed]
    let attachments: [Attachment]?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case conversation
        case tag
        case content, timestamp
        case type, owner, pool, embeds, attachments
    }
}

struct Attachment: Codable {
    let id: String
    let filename: String
    let conversationID, messageID: String
    let size: Int
    let url: String
    let proxyURL: String
    let timestamp: Date
    let width, height: Int
    let owner: Owner

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case filename
        case conversationID
        case messageID
        case size, url
        case proxyURL
        case timestamp, width, height, owner
    }
}

struct Owner: Codable {
    let id: String
    let fullname, email, avatar: String
}

struct Embed: Codable {
    let id: String
    let type: String
    let url: String
    let thumbnail: Thumbnail
}

struct Thumbnail: Codable {
    let url: String
    let width, height: Int
    let proxyURL: String

    enum CodingKeys: String, CodingKey {
        case url, width, height
        case proxyURL
    }
}

struct Pool: Codable {
    let id: String
    let timestamp: Date
    let endAt: Date?
    let options: [Option]

    enum CodingKeys: String, CodingKey {
        case id
        case timestamp
        case endAt
        case options
    }
}

struct Option: Codable {
    let id, poolID: Int
    let content: String

    enum CodingKeys: String, CodingKey {
        case id
        case poolID
        case content
    }
}

enum PopulateMessage: String, CaseIterable {
    case owner
    case embeds
    case conversation
    case attachments
    
    var allasString : String {
        PopulateMessage.allCases.map({$0.rawValue}).joined(separator: ",")
    }
}
