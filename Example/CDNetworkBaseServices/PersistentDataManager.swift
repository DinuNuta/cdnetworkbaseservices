//
//  PersistentDataManager.swift
//  Alamofire5_Demo
//
//  Created by Dinu Coscodan on 4/20/20.
//  Copyright © 2020 Dinu Coscodan. All rights reserved.
//

import Foundation
import KeychainSwift


typealias KYCH = KeychainKeys
enum KeychainKeys: String {
    case sessionToken
    case refreshToken
}

public protocol PersistentDataManaging {
//    func getLocalUser() -> User?
//    func setLocalUser(user:User?)
    
    func getAppToken() -> String?
    func setAppToken(token:String?)

    func getSessionToken() -> String?
    func setSessionToken(sessionToken:String?)
    
    func getAppLanguage() -> String
    func setAppLanguage(language:String)
}

fileprivate var globalAppToken : String? = "92fc0238093ec4612c2327558aa46e58"
fileprivate var globalSessionToken : String? = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNTkxODU5NTgxLCJqdGkiOiI5NzllYTFiMzEzYTg0Yjg3YTU4NDgwZmE0ZTQ1ODZhMiIsInVzZXJfaWQiOiI1ZTk4NWYyODM1NGVmMTRkYWIwZjMwZjAifQ.LjnXb3oABJiVaQTDUm11h585toYoPXEEUnkz2ggqBx8"
fileprivate var globalRefreshToken : String?
//fileprivate var localUser : User?
fileprivate var appLanguage: String = Locale.preferredLanguages[0].components(separatedBy: "-").first ?? "en"


public struct PersistentDataManager : PersistentDataManaging
{
    var keychain: KeychainSwift = KeychainSwift()
    
//    public func getLocalUser() -> User?
//    {
//        return localUser
//    }
//
//    public func setLocalUser(user:User?)
//    {
//        localUser = user
//    }
    
    public func getAppToken() -> String? {
        return globalAppToken
    }
    
    public func setAppToken(token:String?) {
        globalAppToken = token
    }
    
    public func getSessionToken() -> String?{
        if globalSessionToken == nil {
            globalSessionToken = self.keychain.get(KYCH.sessionToken.rawValue)
        }
        return globalSessionToken
    }
    
    public func setSessionToken(sessionToken:String?) {
        globalSessionToken = sessionToken
        if let _token = sessionToken {
            keychain.set(_token, forKey: KYCH.sessionToken.rawValue)
        }else{
            keychain.delete(KYCH.sessionToken.rawValue)
        }
    }
    
    public func getRefreshToken() -> String?{
         if globalRefreshToken == nil {
             globalRefreshToken = self.keychain.get(KYCH.refreshToken.rawValue)
         }
         return globalRefreshToken
     }
     
     public func setRefreshToken(refreshToken:String?) {
         globalRefreshToken = refreshToken
         if let _token = refreshToken {
             keychain.set(_token, forKey: KYCH.refreshToken.rawValue)
         }else{
             keychain.delete(KYCH.refreshToken.rawValue)
         }
     }
    
    public func getAppLanguage() -> String {
        return appLanguage
    }
    
    public func setAppLanguage(language: String) {
        appLanguage = language
        //TODO: save to nsuserDefaults
    }
    
    public init(){}
}

import CDNetworkBaseServices
extension PersistentDataManager: AccessTokenStorage {
    public var refreshToken: JWT? {
        get {
            getRefreshToken()
        }
        set(newValue) {
            setRefreshToken(refreshToken: newValue)
        }
    }
    
    public var accessToken: JWT {
        get {
            return getSessionToken() ?? ""
            //should add error if session token is nil and switch to login screen
        }
        set {
            setSessionToken(sessionToken: newValue)
        }
    }
    
    public var appToken: [String : String]? {
        get {
            return ["appToken": getAppToken() ?? ""]
        }
        set(newValue) {
            setAppToken(token: newValue?["appToken"])
        }
    }
    
}
