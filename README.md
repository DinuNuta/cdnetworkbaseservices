# CDNetworkBaseServices

[![CI Status](https://img.shields.io/travis/DinuNuta/CDNetworkBaseServices.svg?style=flat)](https://travis-ci.org/DinuNuta/CDNetworkBaseServices)
[![Version](https://img.shields.io/cocoapods/v/CDNetworkBaseServices.svg?style=flat)](https://cocoapods.org/pods/CDNetworkBaseServices)
[![License](https://img.shields.io/cocoapods/l/CDNetworkBaseServices.svg?style=flat)](https://cocoapods.org/pods/CDNetworkBaseServices)
[![Platform](https://img.shields.io/cocoapods/p/CDNetworkBaseServices.svg?style=flat)](https://cocoapods.org/pods/CDNetworkBaseServices)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CDNetworkBaseServices is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CDNetworkBaseServices'
```

## Author

DinuNuta, dinu.coscodan@ebs-integrator.com

## License

CDNetworkBaseServices is available under the MIT license. See the LICENSE file for more info.
